import model.{Thing, KnapSack}
import org.scalatest._

class SolversSpec extends FlatSpec with Matchers {

  "The solver" should "solve a problem correctly" in {
//    val thingArray =
//      Array(27, 38, 2, 86, 41, 112, 1, 0, 25, 66, 1, 97, 34, 195, 3, 85, 50, 42, 12, 223)
//    val instance = new KnapSackProblem(thingCount = 10, capacity = 100, idealSolutionValue = 798)
//    instance.loadThings(thingArray.map(_.toString))

    // val thingArray = Array(5, 2, 9, 6, 20, 5, 12, 3, 18, 4)
    val thingArray = Array(2, 5, 6, 9, 5, 20, 3, 12, 4, 18)
    val instance = new KnapSackProblem(thingCount = 5, capacity = 10, idealSolutionValue = 38)
    instance.loadThings(thingArray.map(_.toString))

    val desiredSolution =
      KnapSack.createFromBinary(Integer.parseInt("00101", 2), instance.stuff)

    def testSolver(solver: (List[Thing], Integer) => KnapSack) =
      solver(instance.stuff, instance.capacity).stuff.sortBy(_.value) should
        be (desiredSolution.stuff.sortBy(_.value))

    def testSolverWithValue(solver: (List[Thing], Integer) => Integer) =
      solver(instance.stuff, instance.capacity) should
        be (desiredSolution.getTotalValue)

    // choose a solver to test
    testSolverWithValue(Solvers2.priceDecomposition)
    //testSolver(Solvers.sortedWeightsGreedySolver)
  }

}
