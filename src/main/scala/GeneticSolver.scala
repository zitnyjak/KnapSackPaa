import model.{CustomGAConfiguration, KnapSack, KnapsackFitnessFunction, Thing}
import org.jgap._
import org.jgap.impl._

/**
  * Genetic Solver
  *
  * @author Jakub Zitny <zitnyjak@fit.cvut.cz>
  * @since Fri Oct 30 05:03:43 CET 2015
  */
object GeneticSolver {

  val EvolutionCount = 100
  val PopulationSize = 20
  val CrossoverRate = .2
  val MutationRate = 10

  /**
   * Prepare and evolve the genetic solver.
   *
   * @param stuff List of things to be loaded into the bag
   * @param capacity Integer max capacity of the bag
   * @return KnapSack best solution found by the solver
   */
  def solve(stuff: List[Thing], capacity: Integer): KnapSack = {
    Configuration.reset()

    val conf = new CustomGAConfiguration(CrossoverRate, MutationRate)
    conf.setPreservFittestIndividual(true)
    val func = new KnapsackFitnessFunction(stuff, capacity)
    conf.setFitnessFunction(func)

    val genes: IndexedSeq[Gene] = stuff.indices.map(_ => {
      new BooleanGene(conf, true)
    })

    val sampleChromosome = new Chromosome(conf, genes.toArray)
    conf.setSampleChromosome(sampleChromosome)
    conf.setPopulationSize(PopulationSize)

    val population = Genotype.randomInitialGenotype(conf)
    population.evolve(EvolutionCount)

    constructBestSolution(population, stuff)
  }

  /**
    * Construct the KnapSack from fittest chromosome.
    * TODO: refactor.
    *
    * @param population current evolved population
    * @param stuff List of things to be loaded into the bag
    * @return
    */
  def constructBestSolution(population: Genotype, stuff: List[Thing]): KnapSack = {
    val bestSolutionSoFar = population.getFittestChromosome()

    var bestSol = new KnapSack(List())
    var id = 0
    bestSolutionSoFar.getGenes.foreach {
      case gene: BooleanGene =>
        val present = gene.getAllele.asInstanceOf[Boolean]
        if (present) {
          bestSol = bestSol.addThing(stuff(id))
        }
        id += 1
      case _ => throw new ClassCastException
    }

    bestSol
  }

}
