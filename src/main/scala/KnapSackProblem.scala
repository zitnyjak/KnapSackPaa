import model.{Thing, KnapSack}

/**
 * One instance for calculating the KnapSack problem
 *
 * @param instanceId Int id of the instance
 * @param thingCount Int count of the things available
 * @param capacity Int maximum capacity of the KnapSack
 * @param idealSolutionValue Int value of things in ideally packed KnapSack
 */
case class KnapSackProblem (instanceId: Integer = 0, thingCount: Int,
                            capacity: Integer, idealSolutionValue: Int ) {

  val DebugOutput = false

  var foundSolution: Option[KnapSack] = None
  var foundSolutionValue: Option[Int] = None
  var stuff = List[Thing]()

  /**
   * Load available things for KnapSack in this instance
   *
   * @param thingData Array of Strings with thing from input file
   */
  def loadThings(thingData: Array[String]) = {
    for (i <- 0 to thingCount * 2 - 1 by 2) {
      stuff = stuff :+ new Thing(thingData(i).toInt, thingData(i + 1).toInt)
    }
  }

  /**
   * Dispatcher for finding solutions with given solver
   *
   * @param solver Function taking parsed input and returning a solution
   */
  def findSolution2(solver: (List[Thing], Integer) => Integer) = {
    foundSolutionValue = Option(solver(stuff, capacity))
    if (DebugOutput) println(s"$instanceId $thingCount ${foundSolution.get.getTotalValue} " +
      stuff.map(x => if (foundSolution.get.stuff.contains(x)) 1 else 0).mkString(" "))
    foundSolutionValue
  }

  def findSolution(solver: (List[Thing], Integer) => KnapSack) = {
    foundSolution = Option(solver(stuff, capacity))
    foundSolutionValue = Option(foundSolution.get.getTotalValue)
    if (DebugOutput) println(s"$instanceId $thingCount ${foundSolution.get.getTotalValue} " +
      stuff.map(x => if (foundSolution.get.stuff.contains(x)) 1 else 0).mkString(" "))
    foundSolution
  }

  /**
   * Calculate relative error for max optimization problem
   * (C(OPT)-C(APX)) / C(OPT)
   *
   * @return Double relative error
   */
  def calcError(): Double =
  (idealSolutionValue.toDouble - foundSolution.get.getTotalValue) / idealSolutionValue


  def calcError2(): Double =
    (idealSolutionValue.toDouble - foundSolutionValue.get) / idealSolutionValue

}
