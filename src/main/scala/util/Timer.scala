package util

/**
 * Timing utility object
 *
 * @author Jakub Zitny <zitnyjak@fit.cvut.cz>
 * @since Sat Nov  7 12:29:07 CET 2015
 */
object Timer {

  val DebugTime = false
  val TimeScale = "ms"

  private def systemTimeScaled = TimeScale match {
    case "ms" => System.currentTimeMillis()
    case "ns" => System.nanoTime()
    case _ => System.currentTimeMillis()
  }

  /**
   * measure execution time for given block
   *
   * @param block R block to exec and measure
   * @param blockTag String human readable tag of block to measure (for debugging)
   * @tparam R block
   * @return Tuple_2 of the block's result and the exec time
   * @see inspiration at http://bit.ly/1GQi3Br
   */
  def measureDuration[R](block: => R)
                        (blockTag: String = "block"): (R, Double) = {
    val t0 = systemTimeScaled
    val result = block // call-by-name
    val t1 = systemTimeScaled

    val diff = t1 - t0

    if (DebugTime) println(s"Elapsed time: ($blockTag): $diff$TimeScale")
    (result, diff)
  }

}
