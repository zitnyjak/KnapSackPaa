import model.{Thing, KnapSack}


object Solvers2 {

  /**
    * Dynamic programming, decomposition by price.
    *
    * @param stuff    List of things to be loaded into the bag
    * @param capacity Integer max capacity of the bag
    * @return KnapSack best solution found by the solver
    */


  def capacityDecomposition(stuff: List[Thing], capacity: Integer): KnapSack = {
    def isTrivial(stuff: List[Thing], capacity: Int): Boolean = stuff.isEmpty || capacity <= 0
    def knap(knapSack: KnapSack, stuff: List[Thing], capacity: Int): KnapSack = {
      if (isTrivial(stuff, capacity)) {
        return KnapSack.createEmpty
      }

      val stuffWithoutLast = stuff.dropRight(1)
      val knapSackWithoutLast = knap(knapSack, stuffWithoutLast, capacity)
      val knapSackWithLast = knap(knapSack, stuffWithoutLast, capacity - stuff.last.weight)

      val capacityWithoutLast = stuffWithoutLast.map(_.weight)
      if (knapSackWithLast.getTotalValue + stuff.last.value > knapSackWithoutLast.getTotalValue) {
        return knapSack.addThing(stuff.last)
      } else {
        return knapSackWithLast
      }
    }
    knap(KnapSack.createEmpty, stuff, capacity)
  }


  def printTable(table: Array[Array[Option[Int]]]): Unit = {
    for (i <- 0 until table.size) {
      print(s"${i}:\t")
      for (j <- 0 until table(i).size) {
        table(i)(j) match {
          case None => print("X\t")
          case max if table(i)(j).get == Int.MaxValue => print("MAX\t")
          case _ => print(s"${table(i)(j).get}\t")
        }
      }
      println("")
      println("")
    }
  }

  def findBestValue(table: Array[Array[Option[Int]]], capacity: Integer): Integer = {
    val colWithResult = table.map(_(table(0).size - 1))
    var colIndex = colWithResult.size - 1
    var foundIndex = 0

    while (true) {
      val cell = colWithResult(colIndex)
      if (!cell.isEmpty && cell.get <= capacity) {
        foundIndex = colIndex
        return colIndex
      }
      colIndex -= 1
    }

    return foundIndex
  }

    def priceDecomposition(stuff: List[Thing], capacity: Integer): Integer = {
    val totalValue = stuff.map(_.value).reduceLeft(_ + _)
    val count = stuff.size

    val table = (0 to totalValue).toArray.map((x) => (0 to stuff.size).toArray.map((x) => None: Option[Int]))
    for (i <- 0 to stuff.size) {
      table(0)(i) = Some(0)
    }

    for (c <- 1 to totalValue) {
      for (i <- 1 to stuff.size) {
        val ithThing = stuff(i - 1)

        val left = table(c)(i - 1)
        val makesSenseToLookForRight = (c - ithThing.value >= 0)
        if (makesSenseToLookForRight) {
          // println(s"mstlfr (${c},${i})")
          val right = table(c - ithThing.value)(i - 1)
          if (!right.isEmpty) {
            // println(s"nonempty right${right.get}, ${ithThing.value}")

            val rightVal = right.get + ithThing.weight
            if (left.isEmpty) {
              table(c)(i) = Some(rightVal)
            } else {
              val leftVal = left.get
              table(c)(i) = Some(Math.min(leftVal, rightVal))
            }
          } else {
            table(c)(i) = left
          }
        } else {
          table(c)(i) = left
        }
      }
    }

    // printTable(table)

    return findBestValue(table, capacity)
  }

  def fptas(stuff: List[Thing], capacity: Integer): Integer = {
    val relError: Double = 0.2
    val maxValue = stuff.map(_.value).max
    var k = math.floor((relError * maxValue) / stuff.size).toInt
    if (k == 0) {
      k = 1
//      println("=================== vvv")
//      stuff.map((thing) => {
//        println(s"W ${thing.weight} V: ${thing.value} (k=${k})")
//      })
//      return -123
//      return 0
    }

    val altStuff = stuff.map((thing) => {
      // println(s"W ${thing.weight} V: ${thing.value} => W: ${thing.weight} V: ${thing.value / k} (k=${k})")

      new Thing(thing.weight, thing.value / k)
    })
    return priceDecomposition(altStuff, capacity)
  }

}