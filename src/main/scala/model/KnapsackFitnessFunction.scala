package model

import org.jgap.{FitnessFunction, IChromosome}

/**
  * Construct the FitnessFunction class for JGAP.
  *
  * @param stuff List of things to be loaded into the bag
  * @param capacity Integer max capacity of the bag
  */
class KnapsackFitnessFunction(stuff: List[Thing], capacity: Integer) extends FitnessFunction {
  val MAX_BOUND = 1000000000.0d

  def evaluate(chromosome: IChromosome): Double = {
    var count = 0
    val selectedStuffWeights = this.stuff.indices.map((id) => {
      val gene = chromosome.getGene(id)
      val thing = stuff(id)
      val present = gene.getAllele.asInstanceOf[Boolean]
      var res = 0
      if(present) {
        count += 1
        res = thing.weight
      }

      res
    })
    val totalVolume = selectedStuffWeights.sum

    val volumeDifference = Math.abs(capacity - totalVolume)
    var fitness = 0.0d

    fitness += volumeDifferenceBonus(MAX_BOUND, volumeDifference)
    fitness -= computeItemNumberPenalty(MAX_BOUND, count)

    Math.max(1.0d, fitness)
  }

  /**
    * @see https://goo.gl/tRJdS8
    */
  def volumeDifferenceBonus(a_maxFitness: Double, a_volumeDifference: Double): Double = {
    if (a_volumeDifference == 0) a_maxFitness
    else {
      a_maxFitness / 2 - (a_volumeDifference * a_volumeDifference)
    }
  }

  /**
    * @see https://goo.gl/tRJdS8
    */
  def computeItemNumberPenalty(a_maxFitness: Double, a_items: Int): Double = {
    if (a_items == 0) {
      0
    } else {
      Math.min(a_maxFitness, a_items * a_items)
    }
  }
}
