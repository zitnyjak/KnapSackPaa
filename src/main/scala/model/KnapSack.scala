package model

import scala.annotation.tailrec

/**
 * Immutable KnapSack with stuff
 *
 * @param stuff List of the things in the KnapSack
 */
case class KnapSack (stuff: List[Thing]) {

  /**
   * Immutably adds a thing
   *
   * @param thing Thing to be added
   * @return KnapSack new instance with added thing
   */
  def addThing(thing: Thing) = copy(stuff :+ thing)

  /**
   * Calc total value of the stuff in the knapsack
   * TODO: faster maybe? stuff.map(_.value).sum
   *
   * @return Integer
   */
  def getTotalValue: Integer = {
    @tailrec
    def aggValue(stuff: List[Thing], acc: Integer = 0): Integer =
      stuff match {
        case Nil => acc
        case x :: tail => aggValue(tail, acc + x.value)
      }
    aggValue(stuff)
  }

  /**
   * Calc total weight of the stuff in the knapsack
   * TODO: faster maybe? stuff.map(_.weight).sum
   *
   * @return Integet
   */
  def getTotalWeight: Integer = {
    @tailrec
    def aggWeight(stuff: List[Thing], acc: Integer = 0): Integer =
      stuff match {
        case Nil => acc
        case x :: tail => aggWeight(tail, acc + x.weight)
      }
    aggWeight(stuff)
  }

}

object KnapSack {

  /**
   * Create new empty instance
   *
   * @return KnapSack empty instance
   */
  def createEmpty: KnapSack = new KnapSack(List())

  /**
   * Create new instance from binary string
   * binary sequence indicates which thing to put in the knapsack
   *
   * @param x Int representation of the binary string
   * @param stuff List of Things to choose from
   * @return KnapSack new instance
   */
  def createFromBinary(x: Int, stuff: List[Thing]): KnapSack = {
    val binaryRep =  "0" * (stuff.length - x.toBinaryString.length) + x.toBinaryString
    new KnapSack(
      (stuff, binaryRep.toList)
      .zipped
      .map((item: Thing, cond) => if (cond == '1') item else null)
      .filter(_ != null)
    )
  }
}