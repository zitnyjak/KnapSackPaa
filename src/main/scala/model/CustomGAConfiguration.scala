package model

import org.jgap._
import org.jgap.event._
import org.jgap.util._
import org.jgap.impl._

class CustomGAConfiguration(coRate: Double, moRate: Int)
  extends Configuration("", "") with ICloneable {
  try {
    setBreeder(new GABreeder)
    setRandomGenerator(new StockRandomGenerator)
    setEventManager(new EventManager)
    val selector: BestChromosomesSelector = new BestChromosomesSelector(this, 0.90d)
    selector.setDoubletteChromosomesAllowed(true)

    //val selector = new TournamentSelector(this, 1, 1)
    // val selector = new StandardPostSelector(this)

    addNaturalSelector(selector, false)
    setMinimumPopSizePercent(0)
    setSelectFromPrevGen(1.0d)
    setKeepPopulationSizeConstant(true)
    setFitnessEvaluator(new DefaultFitnessEvaluator)
    setChromosomePool(new ChromosomePool)
    addGeneticOperator(new CrossoverOperator(this, coRate))
    addGeneticOperator(new MutationOperator(this, moRate))

  } catch {
    case e: InvalidConfigurationException =>
      throw new RuntimeException("error")
  }
}
