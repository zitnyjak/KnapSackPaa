import model.{Thing, KnapSack}

import scala.annotation.tailrec

/**
 * available solver functions taking parsed input data and returning best solution found
 *
 * @author Jakub Zitny <zitnyjak@fit.cvut.cz>
 * @since Fri Oct 30 05:03:43 CET 2015
 */
object Solvers {

  /**
    * Dispatch solving via genetic algorithm.
    *
    * @param stuff List of things to be loaded into the bag
    * @param capacity Integer max capacity of the bag
    * @return KnapSack best solution found by the solver
    */
  def geneticSolver(stuff: List[Thing], capacity: Integer): KnapSack = {
    GeneticSolver.solve(stuff, capacity)
  }

  /**
    * Recursive greedy solver with branch and bound optimization.
    *
    * @param stuff List of things to be loaded into the bag
    * @param capacity Integer max capacity of the bag
    * @return KnapSack best solution found by the solver
    */
  def branchAndBound(stuff: List[Thing], capacity: Integer): KnapSack =
    _recursiveGreedySolver(stuff, capacity, true)

  /**
    * Greedy solver with graph 0/1 recursion.
    *
    * @param stuff List of things to be loaded into the bag
    * @param capacity Integer max capacity of the bag
    * @return KnapSack best solution found by the solver
    */
  def recursiveGreedySolver(stuff: List[Thing], capacity: Integer): KnapSack =
    _recursiveGreedySolver(stuff, capacity, false)

  /**
    * TODO: Tailrec this?
    *
    * @param stuff List of things to be loaded into the bag
    * @param capacity Integer max capacity of the bag
    * @param bbOpt Boolean optimize by branch and bound cutting infeasible attempts
    * @return KnapSack best solution found by the solver
    */
  def _recursiveGreedySolver(stuff: List[Thing], capacity: Integer, bbOpt: Boolean): KnapSack = {
    var idealKnapSack: KnapSack = KnapSack.createEmpty
    def recSolver(knapSack: KnapSack, remainingStuff: List[Thing]): Any = {
      val attemptIsFeasible = (knapSack.getTotalWeight <= capacity)
      val attemptIsMoreValuable = (knapSack.getTotalValue > idealKnapSack.getTotalValue)
      if (attemptIsFeasible && attemptIsMoreValuable) {
        idealKnapSack = knapSack
      }

      if (remainingStuff.isEmpty) {
        return
      }

      val remainingSum: Int =
        if (remainingStuff.isEmpty) 0 else remainingStuff.map(_.value).reduceLeft(_ + _)
      val remainingUpperBound = knapSack.getTotalValue + remainingSum

      if (bbOpt && idealKnapSack.getTotalValue >= remainingUpperBound) {
        return
      }

      recSolver(knapSack, remainingStuff.tail) // branch without head
      val newKnapSack = knapSack.addThing(remainingStuff.head)
      recSolver(newKnapSack, remainingStuff.tail) // branch with head
    }
    recSolver(KnapSack.createEmpty, stuff)

    idealKnapSack
  }

  /**
    * Greedy solver with tail recursion.
    *
    * @param stuff List of things to be loaded into the bag
    * @param capacity Integer max capacity of the bag
    * @return KnapSack best solution found by the solver
    */
  def tailRecursiveGreedySolver(stuff: List[Thing], capacity: Integer): KnapSack = {
    @tailrec
    def recSolver(idealKnapSack: KnapSack, combinationsLeft: List[Int]): KnapSack =
      combinationsLeft match {
        case Nil => idealKnapSack
        case head :: tail => {
          val attempt = KnapSack.createFromBinary(head, stuff)

          var newIdealKnapSack = idealKnapSack
          val attemptIsFeasible = (attempt.getTotalWeight <= capacity)
          if (attemptIsFeasible && attempt.getTotalValue > idealKnapSack.getTotalValue) {
            newIdealKnapSack = attempt
          }

          recSolver(newIdealKnapSack, tail)
        }
      }
    val combinations = 0 to 2 << stuff.size - 1
    recSolver(KnapSack.createEmpty, combinations.toList)
  }

  /**
   * The greediest of the greedy solvers.
   *
   * @param stuff List of things to be loaded into the bag
   * @param capacity Integer max capacity of the bag
   * @return KnapSack best solution found by the solver
   */
  def greedySolver(stuff: List[Thing], capacity: Integer): KnapSack = {
    var idealSolution = KnapSack.createEmpty
    for (i <- 0 to 2 << stuff.size - 1) {
      val attempt = KnapSack.createFromBinary(i, stuff)
      if (attempt.getTotalWeight <= capacity &&
        attempt.getTotalValue > idealSolution.getTotalValue) {
        //println("(" + i + ") " + i.toBinaryString + ": got " + attempt.getTotalValue)
        idealSolution = attempt
      }
    }
    idealSolution
  }

  /**
   * TODO
   *
   * @param stuff List of things to be loaded into the bag, already sorted
   * @param capacity Integer max capacity of the bag
   * @return KnapSack found solution
   */
  def dummyHeuristicSolver(stuff: List[Thing], capacity: Integer): KnapSack = {
    @tailrec
    def fillKnapSack(knapSack: KnapSack, stuff: List[Thing]): KnapSack =
      knapSack.getTotalWeight match {
        case empty if stuff.isEmpty => knapSack
        case totalWeight if totalWeight + stuff.head.weight >= capacity => knapSack
        case _ => fillKnapSack(knapSack.addThing(stuff.head), stuff.tail)
      }
    fillKnapSack(KnapSack.createEmpty, stuff)
  }

  /**
   * The dummy heuristic with presorted things by value, descending.
   *
   * @see dummyHeuristic
   */
  def sortedValuesDummyHeuristicSolver(stuff: List[Thing], capacity: Integer): KnapSack =
    dummyHeuristicSolver(stuff.sortBy(-_.value), capacity)

  /**
   * The dummy heuristic with presorted things by weights, ascending.
   *
   * @see dummyHeuristic
   */
  def sortedWeightsDummyHeuristicSolver(stuff: List[Thing], capacity: Integer): KnapSack =
    dummyHeuristicSolver(stuff.sortBy(_.weight), capacity)

  /**
   * The dummy heuristic with presorted things by the ratio of values to weights, descending.
   *
   * @see dummyHeuristic
   */
  def sortedValueWeightsDummyHeuristicSolver(stuff: List[Thing], capacity: Integer): KnapSack =
    dummyHeuristicSolver(stuff.sortBy(_.valueWeightRatio), capacity)

}
